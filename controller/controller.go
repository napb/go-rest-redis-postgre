package controller

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
	"rest-redis-postgre/dto"
	"time"
)

var (
	ctx = context.Background()
)

func GetPerson(c *gin.Context) {

	cache := c.MustGet("cache-connection").(*redis.Client)

	result, err := cache.Get(ctx, "102030").Result()
	if err != nil {
		return
	}

	fmt.Println(result)

	//db := c.MustGet("db-connection").(*gorm.DB)

	/*
		var people []dto.Person
		if err := db.Find(&people).Error; err != nil {
			c.AbortWithStatus(404)
			fmt.Println(err)
		} else {
			c.JSON(200, people)
		}

	*/
}

func GetPersons(c *gin.Context) {
	/*
		cache := c.MustGet("cache-connection").(*redis.Client)
		db := c.MustGet("db-connection").(*gorm.DB)

		id := c.Params.ByName("id")
		var person dto.Person

		val, _ := cache.Get(ctx, id).Result();

		if val != "" {
			json.Unmarshal([]byte(val), &person)
			c.JSON(200, person)
			return
		}

		if err := db.Where("id = ?", id).First(&person).Error; err != nil {
			c.AbortWithStatus(404)
			fmt.Println(err)
		} else {
			cache.Set(ctx, strconv.FormatUint(uint64(person.ID), 10), &person, time.Minute*10)
			c.JSON(200, person)
		}
	*/
}

func CreatePerson(c *gin.Context) {

	cache := c.MustGet("cache-connection").(*redis.Client)
	//db := c.MustGet("db-connection").(*gorm.DB)

	var person dto.Person
	c.BindJSON(&person)

	//cache.Set(ctx, strconv.FormatUint(uint64(person.ID), 10), &person, time.Minute*10)
	cache.Set(ctx, "102030", &person, time.Minute*10)
	//db.Create(&person)
	c.JSON(200, person)
}

func UpdatePerson(c *gin.Context) {

}

func DeletePerson(c *gin.Context) {

}
