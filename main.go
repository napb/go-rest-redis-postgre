package main

import (
	"github.com/gin-gonic/gin"
	"rest-redis-postgre/controller"
	"rest-redis-postgre/middleware"
	"rest-redis-postgre/postgresql"
)

func main() {

	postgresql.DbAutoMigrate()

	r := gin.Default()
	r.Use(
		middleware.DbConnectionMiddleware(),
		middleware.RedisMiddleware(),
	)
	r.GET("/persons", controller.GetPersons)
	r.GET("/persons/:id", controller.GetPerson)
	r.POST("/persons", controller.CreatePerson)
	r.PUT("/persons/:id", controller.UpdatePerson)
	r.DELETE("/persons/:id", controller.DeletePerson)

	err := r.Run(":8080")
	if err != nil {
		return
	}
}
