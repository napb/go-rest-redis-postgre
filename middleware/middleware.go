package middleware

import (
	"github.com/gin-gonic/gin"
	"rest-redis-postgre/redis"
)

func RedisMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("cache-connection", redis.RedisConnection())
		c.Next()
	}
}

func DbConnectionMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("db-connection" /*postgresql.DbConn()*/, "")
		c.Next()
	}
}
