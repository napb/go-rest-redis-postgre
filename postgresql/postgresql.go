package postgresql

import (
	"fmt"
	"rest-redis-postgre/dto"
)
import "gorm.io/gorm"
import "gorm.io/driver/postgres"

func DbConn() *gorm.DB {
	dsn := "host=localhost user=admin password=example dbname=postgres port=5432 sslmode=disable TimeZone=Asia/Shanghai"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		fmt.Println(err)
	}

	return db
}

func DbAutoMigrate() {
	err := DbConn().AutoMigrate(&dto.Person{})
	if err != nil {
		return
	}
}
